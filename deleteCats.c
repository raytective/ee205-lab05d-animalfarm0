///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#include "catDatabase.h"
#include <stdio.h>

void deleteAllCats() {
   currentCats = 0;
}

void deleteCat(int index) {
   printf("%s\n", catNames[index]);
   for(int i=0; i < MAX_NAME; i++) {
      catNames[index][i] = 0;
   }
   printf("name? %s\n", catNames[index]);

}
