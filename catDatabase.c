///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @desc oh boy animal farm! love animal farm! *pain
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#include "catDatabase.h"

int currentCats;

char catNames[MAX_CATS] [MAX_NAME] ;
enum genders catGender[MAX_CATS] ;
enum breeds catBreed[MAX_CATS] ;
bool catFixed[MAX_CATS] ;
float catWeight[MAX_CATS] ;
