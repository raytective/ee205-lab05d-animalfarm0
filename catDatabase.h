///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.h 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <stddef.h>
#include <stdbool.h>

#define MAX_CATS 50
#define MAX_NAME 30
#define BAD_CAT -1

extern int currentCats;

enum genders{ UNKNOWN_GENDER=0, MALE, FEMALE };
enum breeds{ UNKNOWN_BREED=0, MAINE_COON, MANX,
                    SHORTHAIR, PERSIAN, SPHYNX };

extern char catNames[MAX_CATS] [MAX_NAME] ;
extern enum genders catGender[MAX_CATS] ;
extern enum breeds catBreed[MAX_CATS] ;
extern bool catFixed[MAX_CATS] ;
extern float catWeight[MAX_CATS] ;
