///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>

#include "updateCats.h"
#include "catDatabase.h"

int updateCatName(int index, char newName[]) {

   if( strlen(newName) == 0 ) {
      printf("animalFarm0: updateCats.c: Error can't find name.");
      return BAD_CAT;
   }
   for(int i=0; i < currentCats; i++) {
      if(strcmp(newName, catNames[i]) == 0) {
         printf("animalFarm0: updateCats.c: Pre-existing name.\n"); 
         return BAD_CAT;}
      }
#ifdef DEBUG
   printf("index is %d\n", index);
   printf("new name is %s\n", newName);
   printf("old name is %s\n", catNames[index]);
#endif

   for(int i=0; i < MAX_NAME; i++) {
      catNames[index][i] = 0;
   }

   for(unsigned long i=0; i < strlen(newName); i++) {
      catNames[index][i] = newName[i];}
#ifdef DEBUG
   printf("changed name is %s\n", catNames[index]);
#endif
   return 0;
}


int fixCat(int index) {
   
   if(catFixed[index] == true){
      printf("animalFarm0: updateCats.c: Already fixed :(\n");
      return BAD_CAT;
   }

   catFixed[index]=true;
   return 0;
}


int updateCatWeight(int index, float newWeight) {
   if(newWeight <= 0){
      printf("animalFarm0: updateCats.c: Invalid weight\n");
      return BAD_CAT;
   }

   catWeight[index] = newWeight;

   return 0;
}
