///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>

#include "catDatabase.h"

int addCat(char name[],enum genders newGender,enum breeds newBreed,bool fixed,float weight) { 
   int cats = currentCats;

   if( currentCats >= MAX_CATS) {
      printf("animalFarm0: addCats.c: The database is full.\n");
      return BAD_CAT;
   }
   if( strlen(name) <= 0 ) {
      printf("animalFarm0: addCats.c: Empty name.\n");
      return BAD_CAT;
   }
   if( strlen(name) > MAX_NAME ) {
      printf("animalFarm0: addCats.c: Name exceeds maximum length!\n");
      return BAD_CAT;
   }
   if( weight <= 0 ) {
      printf("animalFarm0: addCats.c: Can't have negative weight.\n");
      return BAD_CAT;
   }
   

   // iterate through catNames
   for(int i=0; i < currentCats; i++) {
   // if two strings match   
      if (strcmp(name, catNames[i]) == 0){
         printf("animalFarm0: addCats.c: Name taken!\n");
         return BAD_CAT;}
   }

   for(long unsigned j=0; j <= strlen(name); j++){
      catNames[currentCats][j] = name[j];
   }
   catGender[currentCats] = newGender;
   catBreed[currentCats] = newBreed;
   catFixed[currentCats] = fixed;
   catWeight[currentCats] = weight;

#ifdef DEBUG
   printf("current cat = %d\n", currentCats);
   printf("current cat name = %s\n", catNames[currentCats]);
   printf("length of name = %ld\n", strlen(name));
   printf("gender = %d\n", catGender[currentCats]);
   printf("breed =  %d\n", catBreed[currentCats]); 
   printf("fixed = %d\n", catFixed[currentCats]);
   printf("weight = %f\n", catWeight[currentCats]);
   printf("\n");
#endif

   currentCats++;
   return cats;
   
}
