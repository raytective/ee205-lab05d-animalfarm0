///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.h 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

extern int updateCatName(int index, char newName[]);
extern int fixCat(int index);
extern int updateCatWeight(int index, float newWeight);
