///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c 
/// @version 1.0
///
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date   14_02_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>

#include "catDatabase.h"

void printCat( int index ) {
   
   if(index < 0) {
      printf("animalFarm0: Bad cat [%d]\n", index);
      return;
   }
   if(index > currentCats) {
      printf("animalFarm0: Bad cat [%d]\n", index);
      return;
   }

   printf("cat index = [%d] ", index);
   printf("name=[%s] ", catNames[index]);
   printf("gender=[%d] ", catGender[index]); 
   printf("breed=[%d] ", catBreed[index]);
   printf("isFixed=[%d] ", catFixed[index]);
   printf("weight=[%f]\n",catWeight[index]);

}


void printAllCats() {

   for(int i=0; i < currentCats; i++) {
      printCat(i);
   }

}


int findCat( char name[] ) {
   for(int i=0; i < currentCats; i++) {
      if(strcmp(name, catNames[i]) == 0){
         return i;
      }
   }
   return BAD_CAT;

}
