###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Rachel Watanabe<rkwatana@hawaii.edu>
# @date   21_02_2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = animalFarm0


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)


main.o: main.c catDatabase.h addCats.h reportCats.h updateCats.h
	$(CC) $(CFLAGS) -c main.c


catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c catDatabase.h addCats.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c catDatabase.h reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c catDatabase.h updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c catDatabase.h deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c


animalFarm0: catDatabase.o main.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o

